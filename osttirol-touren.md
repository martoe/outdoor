Bergtouren
==========

## Lienz, Lienzer Dolomiten

### Dolomitenhütte vom Tristacher See (2024)

* Anfahrt: 0:05
* Dauer: 4:30 (5:45 mit Rauchkofel)
* Höhenmeter: 800 (bzw. 1100)
* Referenz: [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/dolomitenhuette/296887689/?share=%7Ezzhgrikx%244ossvipc)

Etwas eintönige, steiler Schotterweg; nur die letzte halbe Stunde ein schöner Wiesenweg

### Schleinitz Sattelköpfe (2018, 2020)

* Anfahrt: 0:15
* Dauer: 6:00 (kann aber abgekürzt werden)
* Höhenmeter: 800
* Referenz: Rother S.168, KSA S.419

Auffahrt mit der Seilbahn Zettersfeld. Unterwegs keine Einkehr
Abkürzung bei den Neualplseen - halbe Zeit. Für die ganze Runde Klettersteigset notwendig

### Böses Weibele (2019)

* Anfahrt: [0:20](https://goo.gl/maps/A94DXC522avrLKQv6)
* Dauer: 3:30
* Höhenmeter: 600
* Referenz: Weiss S.63

via [Hochsteinhütte](https://www.alpenverein.at/lienz/huetten/hochsteinhuette.php)

### Hochschoberhütte, Hochschober (2017, 2019)

* Anfahrt: 0:30
* Dauer: 3:30 (weiter zum Hochschober: insges. 7:00)
* Höhenmeter: 700 bzw 1400
* Referenz: Rother S.129

2017 mit Lamas

### Lienzer Hütte (2019, 2021)

2019 mit Lamas
sehr leicht

### Wangenitzsee von Seichenbrunn (2022)

* Anfahrt: [0:40](https://goo.gl/maps/CiHKvhsTUcUx7idv6)
* Dauer: 5:00
* Höhenmeter: 900
* Referenz: Rother S.174

Holprige Zufahrt. Eher eintöniger Aufstieg
Am See ein kurzer C-Klettersteig (noch nie gemacht)

### Rudl-Eller-Weg (2020)

* Anfahrt: [0:20](https://goo.gl/maps/w9WZEPukfdUDPeb58)
* Dauer: 4:30
* Höhenmeter: 800
* Referenz: [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/rudl-eller-weg/295330780/?share=%7Ezzgmikbv%244ossncvi)

Mautstraße. Einkehr Karlsbader Hütte

### Amlach - Rauchkofel - Dolomitenhütte (?)

* Anfahrt: 0:05
* Dauer: 5:30
* Höhenmeter: 1200
* Referenz: [Tourenfex](https://www.tourenfex.at/bergtouren/rauchkofel/index.php)

### Rotstein (?)

* Anfahrt: [0:30](https://maps.app.goo.gl/Z8ABKYRfyM58po2U8)
* Dauer: 6:00
* Höhenmeter: 1000
* Referenz: Stiller S. 112,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/rotstein/296017308/?share=%7Ezza9nmzm%244ossuaai)

Einkehr beim Huber Kaser (erst gegen Ende)

## Hohe Tauern

### Kasteneck (2020)

* Anfahrt: 0:45
* Dauer: 5:00
* Höhenmeter: 900
* Referenz: Rother S.100,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/lucknerhaus-kasteneck-glorerhuette/296012339/?share=%7Ezzabffwo%244ossuaxg)

Schöne Wanderung, steiler Abstieg zur Glorer Hütte (Einkehr)
Der Gipfel kann auch über den Wiener Höhenweg westlich umgangen werden, 150 Höhenmeter weniger, 15-30 min kürzer (siehe
Still S.142).  
Mautstraße

### Blauspitz (2021)

* Anfahrt: [0:40](https://maps.app.goo.gl/QYTwn5xdXNRFczYi6)
* Dauer: 5:00
* Höhenmeter: 750
* Referenz: Rother S.90,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/kals-blauspitz/295328874/?share=%7Ezzgmtlsh%244ossnchg)

Mit dem Lift zur Bergstation, Abfahrt von der Mittelstation
Zustieg wie Klettersteig, dann Normalweg (etwas schneller). Etwas ausgesetzt

### Matreier Tauernhaus - Grünsee (?)

* Anfahrt: [0:40](https://goo.gl/maps/jBzN9G6Y25ctRssh8)
* Dauer: 4:00 (bzw. 6:30)
* Höhenmeter: 750 (bzw. 1200)
* Referenz: Weiss S.13, Rother S.22

lange Variante weiter auf den Messelingkogel; keine Einkehrmöglichkeit

### Edelweißwiesen, Bretterwandspitze (2024)

* Anfahrt: [0:40](https://maps.app.goo.gl/JNFuFmG71FnMmnk56)
* Dauer: 3:00 (6:30 zur Bretterwandspitze)
* Höhenmeter: 500 (bzw. 1400)
* Referenz: Stiller S.132+134
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/bretterwandspitze/296018058/?share=%7Ezza9wkuz%244ossuabz)

Einkehr: Äußere Steineralm

## Villgratental

### Falkamsee (?)

* Anfahrt: [0:50](https://maps.app.goo.gl/hUbHePmHscNJnyW18)
* Dauer: 4:30
* Höhenmeter: 850
* Referenz: Still S.49,
  [AVaktiv](https://www.alpenvereinaktiv.com/s/IMJseE)

Einkehr nur am Ende.

### Marchkinkele (?)

* Anfahrt: [0:45](https://maps.app.goo.gl/tc5z1FR1QTQH2LMJ8)
* Dauer: 4:30
* Höhenmeter: 900
* Referenz: Still S.30,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/marchhuette/296014749/?share=%7Ezza9addo%244ossuayy)

Einkehr: Marchhütte. Alternative (ohne Einkehr): Kreuzspitze (Still S.27,
[AVaktiv](https://www.alpenvereinaktiv.com/de/tour/kreuzspitze/296014205/?share=%7Ezza94b3p%244ossuayu)), 100
Höhenmeter mehr, selbe Zeit.

## Defreggental

### Speikbodenhütte - Gritzer Hörndle (2023, 2024)

* Anfahrt: [0:50](https://maps.app.goo.gl/khttS4e2Sc2waLrE7)
* Dauer: 4:30 (4:00 ohne Gipfel)
* Höhenmeter: 700 (bzw. 550)
* Referenz: Rother S.108, Still S.166,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/gritzer-hoerndle/296012790/?share=%7Ezzabmbjg%244ossuaxj)

### Jagdhausalm (2022, 2023, 2024)

* Anfahrt: [1:00](https://maps.app.goo.gl/uAC9TXERa3S9xc2W8)
* Dauer: 4:00
* Höhenmeter: 300
* Referenz: Rother S.116,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/jagdhausalm/295328033/?share=%7Ezzgmhaje%244ossncha)

Mautstraße; sehr leicht; nette, urige bewirtschaftete Hütte

## Virgental

### Umbalfälle, Clarahütte (2022, 2024)

* Anfahrt: [0:50](https://maps.app.goo.gl/B1DXzuT6BWBf7Qwm8)
* Dauer: 5:00
* Höhenmeter: 650
* Referenz: Rother S.64, Weiss S.16,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/tourenplanung-am-2024-06-23/295234506/?share=%7Ezzstmu9e%244ossnu4f)

### Prägraten - Berger-See-Hütte (?)

* Anfahrt: [0:45](https://goo.gl/maps/QQntxZau8cYLHzhDA)
* Dauer: 5:00 (bzw. 6:30 über den Bergerkogel)
* Höhenmeter: 950 (bzw. 1350)
* Referenz: Rother S.67, Weiss S.25

### Prägraten - Schmiedleralm (?)

* Anfahrt: [0:40](https://maps.app.goo.gl/qknbpnGJVvZVFyk16)
* Dauer: 3:45 (weiter zur Bonn-Matreier Hütte: 6:45)
* Höhenmeter: 700 (bzw. 1300)
* Referenz:
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/praegraten-schmiedleralm/295232046/?share=%7Ezzstiayk%244ossnu3m),
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/praegraten-schmiedleralm-bonn-matreier-huette/295233023/?share=%7Ezzstbv4g%244ossnu4b)

### Eisseehütte, Eissee (?)

* Anfahrt:
* Dauer: 5:45 (ohne Eissee 4:45)
* Höhenmeter: 1050 (bzw. 850)
* Referenz: Still S.178,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/eisseehuette-eissee/296013427/?share=%7Ezzabupik%244ossuay3)

## Lesachtal

### Obstanser See (?)

* Anfahrt: [0:35](https://maps.app.goo.gl/cVxtEDsK3BQqUnJv8)
* Dauer: 4:45
* Höhenmeter: 900
* Referenz: Still S.72,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/obstanser-see/296016229/?share=%7Ezza9yeck%244ossuaah)

Einkehr: Obstansersee-Hütte. [Obstanser Eishöhle](https://www.alpenvereinaktiv.com/s/3vo3b4) in der Nähe, Besichtigung
nur mit Führung?

### Porze (?)

* Anfahrt: 1:00
* Dauer: 5:30
* Höhenmeter: 1000
* Referenz: Rother S.154, Weiss S.100

inkl. Klettersteig A/B. Einkehr: Porzehütte (Umweg). Für Kinder zu lang

## Italien

### Arzalpenkopf (?)

* Anfahrt: [1:20](https://maps.app.goo.gl/72RPuht86dUAy3YYA)
* Dauer: 4:45
* Höhenmeter: 900
* Referenz: Hülser S.34,
  [AVaktiv](https://www.alpenvereinaktiv.com/de/tour/arzalpenkopf/296082596/?share=%7Ezzacvzxx%244ossuc9a)

Lässt sich auch mit
dem [Arzalpenturm-Klettersteig](https://www.bergsteigen.com/touren/klettersteig/arzalpenturm-klettersteig/) (C)
verbinden.

### Kleiner Lagazuoi - Kaiserjägersteig (?)

* Anfahrt: [1:45](https://maps.app.goo.gl/KhBZDFVTaaxjB6tq9)
* Daumer: 4:30
* Höhenmeter: 800
* Referenz: Hülser S.98,
  [AVaktiv](https://www.alpenvereinaktiv.com/s/3vLaat),
  [Bergsteigen](https://www.bergsteigen.com/touren/klettersteig/via-ferrata-und-tunnel-lagazuoi/)

A-Klettersteig, keine Sicherung notwendig, aber unbedingt Helm und Stirnlampe.

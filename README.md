Osttirol
========

* [Bergtouren](./osttirol-touren.md)
* [Klettersteige](./osttirol-klettersteige.md)
* [Klettern](./osttirol-klettern.md)

Referenzen
----------

* **KidLD** - Klettern in den Lienzer Dolomiten (Armin und Harald Zlöbl, 2. Auflage 2013)
* **KiKuO** - Klettern in Kärnten und Osttirol (Ingo Neumann, 3. Auflage 2016)
* **KSA** - Klettersteig-Atlas Österreich (Kurt Schall, 6. Auflage 2020)
* **Rother** - Rother Wanderbuch Osttirol (Mark Zahel)
* **Weiss** - Bergwandern mit Kindern in Osttirol (Rudolf und Siegrun Weiss)
* **Still** - Stille Wege Osttirol (Edith Kreutner)
* **Hülser** - Auf alten Kriegspfaden durch die Dolomiten (Eugen Hülser)
